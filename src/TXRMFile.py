
from olefile import isOleFile, OleFileIO
import numpy as np
import copy, os

_AcquisitionSettings = {'path':'AcquisitionSettings/',
                       'AcqFileName': 'str',
                       'AcqMode': 'uint32',
                       'Binning': 'uint32',
                       'CollectMultiRef': 'uint8',
                       'CollectSingleRef': 'uint8',
                       'CollectSourceDrift': 'uint8',
                       'Dithering': 'uint8',
                       'EndAngle': 'float32',
                       'ExpTime': 'float32',
                       'ImagePerProj': 'uint32',
                       'IonChamberNormalization': 'uint8',
                       'LoadCellTempControl': 'uint8',
                       'MultiRefInterval': 'uint32',
                       'NameOfSecRefFilter': 'str',
                       'RefCorrModeString': 'str',
                       'RefExpTime': 'float32',
                       'RefFileName': 'str',
                       'RefNumImagesAve': 'uint32',
                       'SampleTooLarge': 'uint32',
                       'StageCorrStatus': 'uint8',
                       'StartAngle': 'float32',
                       'StartPositionZ': 'float32',
                       'TotalImages': 'uint32',
                       'WaitTimeBtwImages': 'float32',
                       'WaitTimeBtwProj': 'float32'}

_ImageInfo = {'path':'ImageInfo/',
             'AbsorptionScaleFactor': 'float32',
             'AbsorptionScaleOffset': 'float32',
             #'AcquisitionMode': 4,
             'Angles': 'float32',
             #'AutoSelectRdOutTime': 1,
             'BGAdjustments': 'float32',
             'CameraName': 'str',
             'CameraNo': 'uint32',
             'CameraNumberOfFramesPerImage': 'uint32',
             'CameraOffset': 'float32',
             #'CameraSensitivity': 4,
             'Current': 'uint32',
             'DataType': 'uint32',
             #'Date': 64040,
             'DtoRADistance': 'float32',
             'Energy': 'float32',
             'ExpTimes': 'float32',
             'FileType': 'uint32',
             #'FocusTarget': 4,
             #'HasImage': 6404,
             'HorizontalBin': 'uint32',
             'ImageHeight': 'uint32',
             'ImageWidth': 'uint32',
             'ImagesPerProjection': 'uint32',
             'ImagesTaken': 'uint32',
             #'IonChamberCurrent': 6404,
             #'IonChamberRead': 4,
             #'LastRecipePoint': 4,
             #'MosaicFastAxis': 4,
             #'MosaicSlowAxis': 4,
             #'MosiacColumns': 4,
             #'MosiacRows': 4,
             #'MostRecentlyWritten': 4,
             'NoOfImages': 'uint32',
             'NoOfImagesAveraged': 'uint32',
             'ObjectiveName': 'str',
             'OpticalMagnification': 'float32',
             #'OriginalDataRefCorrected': 4,
             'PixelSize': 'float32',
             #'ProjectionVersion': 4,
             #'RcpErrMsgFileNo': 4,
             'ReadOutTime': 'float32',
             #'ReadoutFreq': 'float32',
             #'RefTypeToApplyIfAvailable': 4,
             'ReferenceFile': 'str',
             #'SampleStackOrientation': 4,
             #'SourceDriftInterval': 4,
             #'SourceDriftTotal': 4,
             #'SourceFilterIndex': 6404,
             'SourceFilterName': 'str',
             'StoRADistance': 'float32',
             #'Temperature': 4,
             #'ThetaParallax': 6404,
             'TransmissionScaleFactor': 'float32',
             'VerticalalBin': 'uint32',    ### <---- typo!?!?
             'Voltage': 'uint32',
             'XPosition': 'float32',
             'XrayCurrent': 'float32',
             #'XrayGeometry': 4,
             'XrayMagnification': 'float32',
             'XrayVoltage': 'float32',
             'YPosition': 'float32',
             'ZPosition': 'float32',
             }

_ReferenceData = {'path':'ReferenceData/',
                  'BGAdjustments': 'float32',
                  'Binning': 'uint32',
                  'Current': 'uint32',
                  'DataType': 'uint32',
                  'ExpTime': 'float32',
                  'Image': 'float32',
#                  'Image': 'uint16',
                  'ImagesPerProjection': 'uint32',
                  'IonCurrent': 'float32',
                  'RefD2RADistance': 'float32',
                  'RefS2RADistance': 'float32',
                  'SourceFilterIndex': 'uint32',
                  'SourceFilterName': 'str',
                  'XrayCurrent': 'float32',
                  'XrayVoltage': 'float32'
                  }

_MultiReferenceData =  {'path':'MultiReferenceData/',
                        'BGAdjustments': 'float32',
                        'Binning': 'uint32',
                        'Current': 'uint32',
                        'DataType': 'uint32',
                        'ExpTime': 'float32',
                        'HasImage': 'uint8',
                        'ImagesPerProjection': 'uint32',
                        'IonCurrent': 'float32',
                        'RefD2RADistance': 'float32',
                        'RefInterval': 'uint32',
                        'RefS2RADistance': 'float32',
                        'SourceFilterIndex': 'uint32',
                        'SourceFilterName': 'str',
                        'TotalRefImages': 'uint32',
                        'XrayCurrent': 'float32',
                        'XrayVoltage': 'float32'
                        }

_PositionInfo = {'path':'PositionInfo/',
                'AxisNames': 'str',
                'AxisUnits': 'str',
                'MotorPositions': 'float32',
                'NoOfImages': 'uint32',
                'TotalAxis': 'uint32'}

_Alignment = {'path':'Alignment/',
              'EncoderShiftsApplied': 'uint32',
              'EncoderXShifts': 'float32',
              'EncoderYShifts': 'float32',
              'MaximizeVolume': 'uint32',
              'MetrologyShiftsApplied': 'uint32',
              'ReferenceShiftsApplied': 'uint32',
              'ShiftsDataVersion': 'uint32',
              'ShiftsInfoVersion': 'uint32',
              'SourceDriftXShifts': 'float32',
              'SourceDriftYShifts': 'float32',
              'StageShiftsApplied': 'uint32',
              #'StageXShifts': 'float32',
              #'StageYShifts': 'float32',
              'UseSampleOrSrcDrift': 'uint32',
              #'WideModeRelativeXShift': 3204,
              #'WideModeRelativeYShift': 3204,
              'X-Shifts': 'float32',
              'Y-Shifts': 'float32'}

def _traverse_tree(of, path='/'):
    T = of.root.kids_dict
    if not path.lstrip('/') == '':
        path = path.lstrip('/').rstrip('/')
        for d in path.lower().split('/'):
            try:
                T = T[d].kids_dict
            except:
                return None
    return T

# Auxiliary function for listing contents of OLE file
def _ls(of, path='/'):
    T = _traverse_tree(of, path)
    if T is None:
        print("Invalid path: %s" % (path))
        return
    if len(T) == 0:
        print("Not a directory: %s" % (path))
        return
    for k in sorted(T.keys()):
        if T[k].size > 0:
            print("%s (%iB)" % (T[k].name,T[k].size))
        else:
            print(T[k].name)
    return

def _lstree(of, path='/'):
    T = of.root.kids_dict
    if not path.lstrip('/') == '':
        path = path.lstrip('/').rstrip('/')
        for d in path.lower().split('/'):
            try:
                T = T[d].kids_dict
            except:
                print("Invalid path: %s" % (path))
                return

    if len(T) == 0:
        print("Not a directory: %s" % (path))
        return
    print("%s\n \\" % (path))

    stack = sorted(zip(list(T.keys()),list(T.values())),reverse=True)
    indent = len(stack)*[(0,)]
    indent[0] = (indent[0][0],None)
    while len(stack) > 0:
        k,v = stack.pop()
        t = indent.pop()
        if len(t) == 1:
            print("%s |- %s " % (t[0]*" ",v.name))
        else:
            print("%s `- %s " % (t[0]*" ",v.name))
        if len(v.kids_dict) > 0:
            print("%s \\ " % ((4+t[0])*" "))
            T = v.kids_dict
            L = sorted(zip(list(T.keys()),list(T.values())),reverse=True)
            stack.extend(L)
            indent.extend(len(L)*[(t[0]+4,)])
            indent[-len(L)] = (indent[-len(L)][0],None)
    return

# Auxiliary function for reading OLE file entry into Numpy array
def _read_entry(olefile, entry, dtype):
    try:
        s = olefile.openstream(entry)
        val = s.read()
        s.close()
    except:
        print("Failed to read entry: %s" % (entry))
        return None
    else:
        if dtype == 'str':
            return val
        else:
            val = np.fromstring(val, dtype)
    if len(val) == 1:
        return val[0]
    else:
        return val

def _dict_read(olefile, d):
    D = copy.deepcopy(d)
    path = D.pop('path')
    if _traverse_tree(olefile,path) is None: return None
    for key,dtype in D.items():
        D[key] = _read_entry(olefile,path+key,dtype)
    return D


class TXRMFile(object):

    def __init__(self, filename):

        if not isOleFile(filename):
            raise TypeError("Not a valid OLE2 file: %s" % (filename))
        self._olefile = OleFileIO(filename)
        self.AcquisitionSettings = _dict_read(self._olefile, _AcquisitionSettings)
        self.Alignment = _dict_read(self._olefile, _Alignment)
        self.ImageInfo = _dict_read(self._olefile, _ImageInfo)
        self.ReferenceData = _dict_read(self._olefile, _ReferenceData)
        if self.ReferenceData and self.ReferenceData['Image'] is not None:
            try:
                self.ReferenceData['Image'] = np.reshape(self.ReferenceData['Image'], (self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']))
            except:
                _ReferenceData['Image'] = 'uint16'
                self.ReferenceData = _dict_read(self._olefile, _ReferenceData)
                self.ReferenceData['Image'] = np.reshape(self.ReferenceData['Image'], (self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']))
        if self.AcquisitionSettings['CollectMultiRef']:
            self.MultiReferenceData = _dict_read(self._olefile, _MultiReferenceData)
            for k in range(1,self.MultiReferenceData['TotalRefImages']+1):
                self.MultiReferenceData['Image'+str(k)] = np.reshape(_read_entry(self._olefile,'MultiReferenceData/Image'+str(k),np.float32),(self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']))
        else:
            self.MultiReferenceData = None
        self.PositionInfo = _dict_read(self._olefile, _PositionInfo)
        return

    def ls(self, path = '/'):
        _ls(self._olefile, path = path)

    def lstree(self, path = '/'):
        _lstree(self._olefile, path = path)

    def read_entry(self, entry, dtype):
        return _read_entry(self._olefile, entry, dtype)

    def close(self):
        self._olefile.close()

    def proj_data(self, k = None):

        if k is not None:
            if k < 1 or k > self.ImageInfo['ImagesTaken']: raise IndexError("index out of range")
            imdir = (k-1)//100 + 1
            data = np.reshape(_read_entry(self._olefile,'ImageData%i/Image%i' % (imdir,k), np.uint16), (self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']))
        else:
            data = np.ndarray((self.ImageInfo['ImagesTaken'],self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']), dtype=np.uint16)
            for k in range(1,self.ImageInfo['ImagesTaken']+1):
                imdir = (k-1)//100 + 1
                data[k-1,:,:] = np.reshape(_read_entry(self._olefile,'ImageData%i/Image%i' % (imdir,k), np.uint16), (self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']))

        return data

    def ref_data(self, k = None):
        if not self.MultiReferenceData:
            ref = self.ReferenceData['Image']
        else:
            if k is None:
                ref = np.ndarray((self.MultiReferenceData['TotalRefImages'],self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']), dtype = np.float32)
                for k in range(self.MultiReferenceData['TotalRefImages']):
                    ref[k,:,:,] = self.MultiReferenceData['Image%i' % (k+1,)]
            else:
                ref = self.MultiReferenceData['Image%i' % (k,)]
        return ref


    def trans_data(self, k = None):

        if k is None:
            data = self.proj_data().astype(np.float32)
            ref = self.ref_data()
            if not self.MultiReferenceData:
                data = data/ref
            else:
                refint = self.MultiReferenceData['RefInterval']
                numref = self.MultiReferenceData['TotalRefImages']
                for k in range(self.ImageInfo['ImagesTaken']):
                    j = min(numref-1, (k - 1 + int(np.floor(refint/2.))) // refint)
                    data[k,:,:] /= ref[j,:,:]
        else:
            P = self.proj_data(k)
            if not self.MultiReferenceData:
                ref = self.ReferenceData['Image']
            else:
                refint = self.MultiReferenceData['RefInterval']
                numref = self.MultiReferenceData['TotalRefImages']
                j = min(numref, (k - 1 + int(np.floor(refint/2.))) // refint + 1)
                ref = self.MultiReferenceData['Image'+str(j)]
            data = P/ref
        return data


    def export_tiff(self, fname, ref=False, **kwargs):
        import tifffile
        if ref is False:
            tifffile.imsave(fname, self.proj_data(), **kwargs)
        else:
            tifffile.imsave(fname, self.ref_data(), **kwargs)
        return

    def export_hdf5(self, fname, **kwargs):
        import h5py
        hf = h5py.File(fname,'w')
        hf.create_dataset('implements', data = 'measurement:exchange')

        #### Exchange group
        hf.create_group('exchange')

        # Projection angles
        hf.create_dataset('/exchange/theta',data = self.ImageInfo['Angles'])
        hf['exchange/theta'].attrs['units'] = 'degree'

        # Detector shifts
        hf.create_dataset('/exchange/detector_shift_x', data = self.Alignment['X-Shifts'])
        hf['exchange/detector_shift_x'].attrs['units'] = 'pixels'
        hf.create_dataset('/exchange/detector_shift_y', data = self.Alignment['Y-Shifts'])
        hf['exchange/detector_shift_y'].attrs['units'] = 'pixels'

        # Pixel size
        hf.create_dataset('/exchange/pixel_size', data = 1e-6*self.ImageInfo['PixelSize'])
        hf['/exchange/pixel_size'].attrs['units'] = 'm'

        #### Measurement group
        hf.create_group('measurement')

        # Instrument subgroup
        hf.create_group('/measurement/instrument')
        hf.create_dataset('/measurement/instrument/name', data = 'Xradia Micro-CT')
        hf.create_dataset('/measurement/instrument/optical_magnification', data = self.ImageInfo['OpticalMagnification'])

        # Detector subgroup
        hf.create_group('/measurement/instrument/detector')
        hf.create_dataset('/measurement/instrument/detector/model', data = _read_entry(self._olefile,'ConfigureBackup/ConfigCamera/Camera 1/CameraName','str').strip(b'\x00').decode('utf-8'))
        hf.create_dataset('/measurement/instrument/detector/x_pixel_size', data = 1e-6*_read_entry(self._olefile,'ConfigureBackup/ConfigCamera/Camera 1/CCDPixelSize','float32'))
        hf['/measurement/instrument/detector/x_pixel_size'].attrs['units'] = 'm'
        hf.create_dataset('/measurement/instrument/detector/y_pixel_size', data = 1e-6*_read_entry(self._olefile,'ConfigureBackup/ConfigCamera/Camera 1/CCDPixelSize','float32'))
        hf['/measurement/instrument/detector/y_pixel_size'].attrs['units'] = 'm'
        hf.create_dataset('/measurement/instrument/detector/x_binning', data = self.AcquisitionSettings['Binning'])
        hf.create_dataset('/measurement/instrument/detector/y_binning', data = self.AcquisitionSettings['Binning'])
        hf.create_dataset('/measurement/instrument/detector/exposure_time', data = self.AcquisitionSettings['ExpTime'])
        hf['/measurement/instrument/detector/exposure_time'].attrs['units'] = 's'
        if self.AcquisitionSettings['CollectMultiRef']:
            hf.create_dataset('/measurement/instrument/detector/distance', data = 1e-3*self.MultiReferenceData['RefD2RADistance'])
            hf.create_dataset('/measurement/instrument/detector/exposure_time_ref', data = self.MultiReferenceData['ExpTime'])
        else:
            hf.create_dataset('/measurement/instrument/detector/distance', data = 1e-3*self.ReferenceData['RefD2RADistance'])
            hf.create_dataset('/measurement/instrument/detector/exposure_time_ref', data = self.ReferenceData['ExpTime'])
        hf['/measurement/instrument/detector/exposure_time_ref'].attrs['units'] = 's'
        hf['/measurement/instrument/detector/distance'].attrs['units'] = 'm'

        # Source subgroup
        hf.create_group('/measurement/instrument/source')
        if self.AcquisitionSettings['CollectMultiRef']:
            hf.create_dataset('/measurement/instrument/source/distance', data = 1e-3*self.MultiReferenceData['RefS2RADistance'])
            hf.create_dataset('/measurement/instrument/source/current', data = 1e-6*self.MultiReferenceData['XrayCurrent'])
            hf.create_dataset('/measurement/instrument/source/voltage', data = 1e3*self.MultiReferenceData['XrayVoltage'])
        else:
            hf.create_dataset('/measurement/instrument/source/distance', data = 1e-3*self.ReferenceData['RefS2RADistance'])
            hf.create_dataset('/measurement/instrument/source/current', data = 1e-6*self.ReferenceData['XrayCurrent'])
            hf.create_dataset('/measurement/instrument/source/voltage', data = 1e3*self.ReferenceData['XrayVoltage'])
        hf['/measurement/instrument/source/distance'].attrs['units'] = 'm'
        hf['/measurement/instrument/source/current'].attrs['units'] = 'A'
        hf['/measurement/instrument/source/voltage'].attrs['units'] = 'V'

        # White field
        hf.create_dataset('/exchange/data_white',data = self.ref_data())
        hf['exchange/data_white'].attrs['units'] = 'counts'
        if self.AcquisitionSettings['CollectMultiRef']:
            hf['exchange/data_white'].attrs['multiref_interval'] = self.AcquisitionSettings['MultiRefInterval']
            hf['exchange/data_white'].attrs['images_per_projection'] = self.MultiReferenceData['ImagesPerProjection']
        else:
            hf['exchange/data_white'].attrs['images_per_projection'] = self.ReferenceData['ImagesPerProjection']

        # Dark field
        hf.create_dataset('/exchange/data_dark', data = np.zeros((self.ImageInfo['ImageHeight'],self.ImageInfo['ImageWidth']),dtype=np.float))
        hf['exchange/data_dark'].attrs['units'] = 'counts'

        # Projection data
        hf.create_dataset('/exchange/data',data = self.proj_data())
        hf['exchange/data'].attrs['units'] = 'counts'
        hf['exchange/data'].attrs['axes'] = 'theta:y:x'

        ## Close hdf5 file
        hf.close()
