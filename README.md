# TXRM Tools

The purpose of this Python module is to extract information from `.txrm` data files from *ZEISS Xradia Versa* 3D X-ray microscopy scanners. The file format is essentially a Microsoft [OLE2 container](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-oleds/fdc5e702-d09e-4344-a77f-eb079d41f23f) with a proprietary hierarchical structure that is specific to a family of ZEISS scanners.

**Disclaimer**
This module is based on *reverse engineering* rather than a formal specification—users should exercise caution when using this module. Use at your own risk!

## License

[BSD 2-Clause](LICENSE)


## Installation

The latest version of TXRM Tools (and all dependencies) can be installed with [pip](https://pip.pypa.io) as follows:

```
$ pip install git+https://gitlab.gbar.dtu.dk/mskan/txrmtools.git
```

This installs the `txrmtools` Python package and a command-line interface with the same name.

## Getting started with the command-line interface

The command-line interface can be used for the following tasks:

- print file info (JSON format)
- list file contents
- print list of projection angles
- print list of projection (x,y) shifts
- export data to a HDF5 file or a TIFF stack

Use `txrmtools -h` to print the CLI help text.

### Exporting data to HDF5

The following example demostrates how to export data from a `.txrm` file to a HDF5 file:

```
$ txrmtools datafile.txrm --hdf5 -o datafile.h5
```

The output file `datafile.h5` is an HDF5 file with a structure that is based on the [DXchange](https://github.com/data-exchange/dxchange) format.


### Exporting data to TIFF stack

The following example demostrates how to export data from a `.txrm` file to a TIFF stack:

```
$ txrmtools datafile.txrm --tiff -o datafile.tiff
```

In addition to the TIFF stack `datafile.tiff`, this will also generate a TIFF file `datafile_ref.tiff` with the flatfield image(s).

## Bugs

If you think that you have found a bug, please file a new issue in the issue tracker.
