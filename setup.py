from setuptools import setup
import versioneer

setup(name='txrmtools',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      install_requires = ['docopt','h5py','numpy','olefile','tifffile'],
      description='Tools for converting and extracting information from data files from ZEISS Xradia Versa 3D X-ray microscopy scanners',
      url='http://gitlab.gbar.dtu.dk/mskan/txrmtools',
      author='Martin S. Andersen',
      author_email='mskan@dtu.dk',
      license='BSD 2-Clause',
      packages = ["txrmtools"],
      package_dir = {"txrmtools": "src"},
      scripts=['bin/txrmtools'],
      zip_safe=True)
